package com.seedling.littles;

import android.os.Bundle;
import android.util.Log;

import eu.kudan.kudan.ARActivity;
import eu.kudan.kudan.ARAlphaVideoNode;
import eu.kudan.kudan.ARImageTrackable;
import eu.kudan.kudan.ARImageTracker;
import eu.kudan.kudan.ARVideoTexture;

public class MaskAR extends ARActivity {

    private ARImageTrackable trackable;

    String name = null;
    ARVideoTexture videoTexture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mask_ar);


    }

    @Override
    public void setup() {
        super.setup();

        name = getIntent().getStringExtra("trackName");
        Log.v("name", "Name = " + name);

        addImageTrackable(name);
        addAlphaVideoNode();
//        addImageNode();
    }

    private void addImageTrackable(String name) {

        // Initialise image trackable
        trackable = new ARImageTrackable("mask");

        //NEW KISS STARS
        trackable.loadFromAsset(name);

        // Get instance of image tracker manager
        ARImageTracker trackableManager = ARImageTracker.getInstance();

        // Add image trackable to image tracker manager
        trackableManager.addTrackable(trackable);

    }


    private void addAlphaVideoNode() {

        // Initialise video texture
        videoTexture = new ARVideoTexture();
        videoTexture.loadFromAsset("marketbox.mp4");

        // Initialise alpha video node with video texture
        ARAlphaVideoNode alphaVideoNode = new ARAlphaVideoNode(videoTexture);

        // Add alpha video node to image trackable
        trackable.getWorld().addChild(alphaVideoNode);

        // Alpha video scale
        float scale = trackable.getWidth() / videoTexture.getWidth();
        alphaVideoNode.scaleByUniform(scale);

        alphaVideoNode.setVisible(true);
    }


    @Override
    protected void onDestroy() {
        if (videoTexture != null) {
            try {
                videoTexture.spill();
            } catch (Exception e){
                Log.e("VidTexture", "MediaPlayer.isPlaying() on null object - no spill() necessary");
            }
        }
        super.onDestroy();
    }
}
