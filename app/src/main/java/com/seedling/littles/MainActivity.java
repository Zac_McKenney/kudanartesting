package com.seedling.littles;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import eu.kudan.kudan.ARAPIKey;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE = 0x420;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ARAPIKey key = ARAPIKey.getInstance();

        //TODO add an API key here to run the project
        key.setAPIKey("ADD API KEY HERE");

    }


    public void maskClick (View view){

        if (requestPermissions(this)) {

            int viewId = view.getId();
            Intent intent = new Intent(this, MaskAR.class);

            switch (viewId) {
                case R.id.sleepMask1:
                    intent.putExtra("trackName", "parkerSleepMaskMarkerA.png");
                    break;
                case R.id.sleepMask2:
                    intent.putExtra("trackName", "parkerSleepMaskMarkerB.png");
                    break;
                case R.id.sleepMask3:
                    intent.putExtra("trackName", "forZac.png");
                    break;
                case R.id.outline1:
                    intent.putExtra("trackName", "outlinekissStar1.png");
                    break;
                case R.id.outline2:
                    intent.putExtra("trackName", "outlinekissStar2.png");
                    break;
                case R.id.outline3:
                    intent.putExtra("trackName", "outlinekissStar3.png");
                    break;
                case R.id.zzz1:
                    intent.putExtra("trackName", "spaceMarker.jpg");
                    break;
                case R.id.zzz2:
                    intent.putExtra("trackName", "sleepMaskCrazyTime.jpg");
                    break;
                case R.id.zzz3:
                    intent.putExtra("trackName", "sleepMaskCrazyTime2.jpg");
                    break;
                case R.id.tum:
                    intent.putExtra("trackName", "alphasurround.png");
                    break;
                case R.id.tumCrop:
                    intent.putExtra("trackName", "tum.png");
                    break;

            }

            startActivity(intent);
        }
    }



    public static Boolean requestPermissions(Context c)
    {
        String permission1 = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        if (ContextCompat.checkSelfPermission(c, permission1) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions((Activity)c,
                    new String[]{permission1},
                    REQUEST_CODE);
        }

        String permission2 = Manifest.permission.READ_EXTERNAL_STORAGE;
        if (ContextCompat.checkSelfPermission(c, permission2) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions((Activity)c,
                    new String[]{permission2},
                    REQUEST_CODE);
        }

        String permission3 = Manifest.permission.CAMERA;
        if (ContextCompat.checkSelfPermission(c, permission3) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions((Activity)c,
                    new String[]{permission3},
                    REQUEST_CODE);
        }

        String permission4 = Manifest.permission.INTERNET;
        if (ContextCompat.checkSelfPermission(c, permission4) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions((Activity)c,
                    new String[]{permission4},
                    REQUEST_CODE);
        }

        String permission5 = Manifest.permission.ACCESS_NETWORK_STATE;
        if (ContextCompat.checkSelfPermission(c, permission5) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions((Activity)c,
                    new String[]{permission5},
                    REQUEST_CODE);
        }

        String permission6 = Manifest.permission.READ_PHONE_STATE;
        if (ContextCompat.checkSelfPermission(c, permission6) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions((Activity)c,
                    new String[]{permission6},
                    REQUEST_CODE);
        }


        return (ContextCompat.checkSelfPermission(c, permission1)
                == PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(c, permission2)
                        == PackageManager.PERMISSION_GRANTED &&
                        (ContextCompat.checkSelfPermission(c, permission3)
                                == PackageManager.PERMISSION_GRANTED) &&
                        (ContextCompat.checkSelfPermission(c, permission4)
                                == PackageManager.PERMISSION_GRANTED)
                &&
                        (ContextCompat.checkSelfPermission(c, permission5)
                                == PackageManager.PERMISSION_GRANTED)
                 &&
                        (ContextCompat.checkSelfPermission(c, permission6)
                                == PackageManager.PERMISSION_GRANTED)

                );
    }
}
